# read file
with open("day18.txt", "rt") as myfile:
   file = myfile.readlines()

# parse problem
cubes=[] # list of all cubes
max_x=0
max_y=0 # max values
max_z=0
for row in file: # build the cubes
    numbers=row.strip().split(',')
    x=int(numbers[0])
    y=int(numbers[1])
    z=int(numbers[2])
    cubes.append((x,y,z))
    max_x=max(max_x,x)
    max_y = max(max_y, y)
    max_z = max(max_z, z)

matrix=[ [ [0 for _ in range(max_z+2)] for _ in range(max_y+2)] for _ in range(max_x+2)]

for x,y,z in cubes:
    matrix[x][y][z]=1 # build the cube and mark every position with a 1

#part 1
count=0
for x,y,z in cubes: # check for every cube if one neighbor is empty
    if matrix[x-1][y][z]==0:
        count+=1
    if matrix[x][y-1][z]==0:
        count+=1
    if matrix[x][y][z-1]==0:
        count+=1
    if matrix[x+1][y][z]==0:
        count+=1
    if matrix[x][y+1][z]==0:
        count+=1
    if matrix[x][y][z+1]==0:
        count+=1
print(count) # 3610


#part 2
outside_connection=[ [ [0 for _ in range(max_z+2)] for _ in range(max_y+2)] for _ in range(max_x+2)]
outside_connection[0][0][0]=1 # build a new cube with outside connection
change=True
while change: # iterate while new outside connected cube
    change=False
    for x in range(len(outside_connection)):
        for y in range(len(outside_connection[x])):
            for z in range(len(outside_connection[x][y])):
                if outside_connection[x][y][z] == 0 and  matrix[x][y][z]==0 and ( # only if no cube and not outside connected
                        (x>0 and outside_connection[x-1][y][z]==1) or (x<len(outside_connection)-1 and outside_connection[x+1][y][z]==1)or
                        (y>0 and outside_connection[x][y-1][z]==1)or (y<len(outside_connection[x])-1 and outside_connection[x][y+1][z]==1)or
                        (z>0 and outside_connection[x][y][z-1]==1)or (z<len(outside_connection[x][y])-1 and outside_connection[x][y][z+1]==1)):
                    outside_connection[x][y][z] = 1 # set to 1
                    change = True

count=0
for x,y,z in cubes: # count the outside connections for each cube
    if outside_connection[x-1][y][z]==1:
        count+=1
    if outside_connection[x][y-1][z]==1:
        count+=1
    if outside_connection[x][y][z-1]==1:
        count+=1
    if outside_connection[x+1][y][z]==1:
        count+=1
    if outside_connection[x][y+1][z]==1:
        count+=1
    if outside_connection[x][y][z+1]==1:
        count+=1
print(count)
# 2082