#read lines of the file
with open("day02.txt", "rt") as myfile:
    game = myfile.readlines()

#mapping for the values
mapping = {
        'A': 0,
        'B': 1,
        'C': 2,
        'X': 0,
        'Y': 1,
        'Z': 2,
    }

#First part
score=0 # the total score
for g in game:
    g = g.strip().split(' ') # get the line and split on withe space
    ob = mapping.get(g[0]) # map both to a value
    my =mapping.get(g[1])
    if ob == my:
        reward=3    # if both equal reward = 3
    elif (ob+1)%3 == my: # winning if step 1 dif
        reward =6   # if win reward = 6
    else:
        reward =0   # reward =0 if lose
    score += my+1+reward # score add is reward+ 1 + my choice
print(score) # 13005 print the score

#Second Part
score=0
for g in game:
    g = g.strip().split(' ')# get the line and split on withe space
    ob = mapping.get(g[0])# map both to a value
    my = mapping.get(g[1])

    if my == 0:
        reward = 0 # if I should lose
        mys = (ob - 1)%3 # my such I lose
    elif my == 1:
        reward=3 # if I should draw
        mys=ob # my such I draw
    else:
        reward=6 # if I should win
        mys=(ob + 1)%3 # my such I win
    score += mys + 1 + reward # score add is reward+ 1 + my choice
print(score) # 11373