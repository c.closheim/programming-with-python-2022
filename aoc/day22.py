with open("day22.txt", "rt") as myfile:
   file = myfile.readlines()

#parse problem
idx=file.index('\n')
maplist=file[:idx]
instructionslist=file[idx+1:][0]

map=[]
maxlen=0
for line in maplist:
    line=line.replace('\n','')
    maxlen=max(maxlen,len(line))
    row=[]
    for char in line:
        if char==' ':
            row.append(1)
        elif char=='#':
            row.append(2)
        else:
            row.append(0)
    map.append(row)

for line in map:
    if len(line)<maxlen:
        toadd=maxlen-len(line)
        for _ in range(toadd):
            line.append(1)



position=map[0].index(0)
startposition=(0,position,(0,1))
weight=len(map[0])
height=len(map)
instructions=[]
instructionslist=instructionslist.strip()
count=0

for i in range(len(instructionslist)):
    if instructionslist[i].isnumeric():
        if i+1< len(instructionslist) and instructionslist[i+1].isnumeric():
            count=count*10+ int(instructionslist[i])
        else:
            instructions.append(count*10+ int(instructionslist[i]))
            count=0
    else:
        instructions.append(instructionslist[i])

#part1
position = startposition
for instruct in instructions:
    if isinstance(instruct , int):
        for _ in range(instruct):

            next_position = ((position[0]+position[2][0])%height,(position[1]+position[2][1])%weight,position[2])

            new_position_found=False
            while not new_position_found:
                if map[next_position[0]][next_position[1]]==0:
                    position=next_position
                    new_position_found = True
                elif map[next_position[0]][next_position[1]]==1:
                    next_position = (
                    (next_position[0] + next_position[2][0]) %height, (next_position[1] + next_position[2][1]) %weight, position[2])
                else:
                    new_position_found = True
    else:
        if instruct=='R':
            if position[2]==(0,1):
                position=(position[0], position[1],(1,0))
            elif position[2]==(1,0):
                position = (position[0], position[1], (0, -1))
            elif position[2] == (0, -1):
                position = (position[0], position[1], (-1, 0))
            elif position[2] == (-1, 0):
                position = (position[0], position[1], (0, 1))
        else:
            if position[2]==(0,1):
                position=(position[0], position[1],(-1,0))
            elif position[2]==(1,0):
                position = (position[0], position[1], (0, 1))
            elif position[2] == (0, -1):
                position = (position[0], position[1], (1, 0))
            elif position[2] == (-1, 0):
                position = (position[0], position[1], (0, -1))

if position[2]==(0,1):
    third = 0
elif position[2]==(1,0):
    third = 1
elif position[2]==(0,-1):
    third = 2
else:
    third=3


print(1000*(position[0]+1)+4*(position[1]+1)+third)


#part 2


class squares:
    def __init__(self, map):
        self.map=map
        self.edge = len(map)
    def set_right(self, squares, direction):
        self.right=squares
        self.right_direction=direction

    def set_left(self, squares, direction):
        self.left=squares
        self.left_direction = direction

    def set_up(self, squares, direction):
        self.up=squares
        self.up_direction = direction

    def set_down(self, squares, direction):
        self.down=squares
        self.down_direction = direction


    def modify_pos_dir(self, x, y, dir_in, dir_out):
        if dir_in== "r":
            if dir_out == "r":
                return self.edge-1-x,self.edge-1,-1,0
            elif dir_out == "d":
                return self.edge-1,x,0,-1
            elif dir_out == "l":
                return x,0,0,1
            elif dir_out == "u":
                return 0,self.edge-1-x,-1,0
        elif dir_in== "d":
            if dir_out == "r":
                return y, self.edge-1, -1, 0
            elif dir_out == "d":
                return self.edge - 1, self.edge-1-y, 0, -1
            elif dir_out == "l":
                return self.edge-1-y, 0, 0, 1
            elif dir_out == "u":
                return 0, y, -1, 0
        elif dir_in== "l":
            if dir_out == "r":
                return x, self.edge - 1, -1, 0
            elif dir_out == "d":
                return self.edge - 1,self.edge-1-x , 0, -1
            elif dir_out == "l":
                return self.edge-1-x , 0, 0, 1
            elif dir_out == "u":
                return 0,x , -1, 0
        elif dir_in== "u":
            if dir_out == "r":
                return self.edge-1-y, self.edge - 1, -1, 0
            elif dir_out == "d":
                return self.edge - 1,y, 0, -1
            elif dir_out == "l":
                return y, 0, 0, 1
            elif dir_out == "u":
                return 0,self.edge-1-y, -1, 0

    def move(self, x,y,d_x,d_y):

        new_x=x+d_x
        new_y=y+d_y
        new_d_x=d_x
        new_d_y=d_y
        square=self
        if new_x!=x: # Vertical move
            if  new_x<0:
                square=self.up
                new_x, new_y, new_d_x, new_d_y = self.modify_pos_dir(x, y, self.up_direction, "u")
            elif  new_x>=self.edge:
                square = self.down
                new_x, new_y, new_d_x, new_d_y = self.modify_pos_dir(x, y, self.down_direction, "d")
        else: #horizontal move:
            if  new_y<0:
                square = self.left
                new_x, new_y, new_d_x, new_d_y = self.modify_pos_dir(x, y, self.left_direction, "l")
            elif  new_y>=self.edge:
                square = self.right
                new_x, new_y, new_d_x, new_d_y = self.modify_pos_dir(x, y, self.right_direction, "r")

        return square, new_x,new_y,new_d_x,new_d_y


class cube:
    def __init__(self):
        self.net_patterns=[
            # [[1,0,0,0],
            #  [2,3,4,5],
            #  [6,0,0,0]],
            # [[0, 1, 0, 0],
            #  [2, 3, 4, 5],
            #  [6, 0, 0, 0]],
            # [[0, 0, 1, 0],
            #  [2, 3, 4, 5],
            #  [6, 0, 0, 0]],
            # [[0, 0, 0, 1],
            #  [2, 3, 4, 5],
            #  [6, 0, 0, 0]],
            # [[0, 0, 1, 0],
            #  [5, 2, 3, 4],
            #  [0, 0, 6, 0]],
            # [[0, 0, 1, 0],
            #  [5, 2, 3, 4],
            #  [0, 6, 0, 0]],
            # [[0, 1, 0, 0],
            #  [0, 3 , 4, 5],
            #  [2, 6, 0, 0]],
            # [[0,0, 3, 4, 5],
            #  [1,2, 6, 0, 0]],
            # [[0, 0, 1, 0],
            #  [0, 3, 4, 5],
            #  [2, 6, 0, 0]],
            # [[0, 0, 0, 1],
            #  [0, 3, 4, 5],
            #  [2, 6, 0, 0]],
            # [[0, 0, 1, 5],
            #  [0, 3, 4, 0],
            #  [2, 6, 0, 0]]
            [[0,0,1,0],
             [5,4,3,0],
             [0,0,6,2]]
        ]

    def pattern_blow_up(self, pattern, size):
        blowup=[]
        for row in pattern:
            blowuprow=[]
            for field in row:
                for _ in range(size):
                    blowuprow.append(field)
            for _ in range(size):
                blowup.append(blowuprow)
        return blowup

    def rotation(self, pat):
        patt=[]
        for c in range(len(pat[0])):
            row = []
            for r in range(len(pat)):
                row.append(pat[r][c])
            patt.append(row[::-1])
        return patt

    def allpatterns(self):
        patterns=[]
        for pattern in self.net_patterns:
            patterns.append(pattern)
            patterns.append(pattern[::-1])
            first_rotation=self.rotation(pattern)
            patterns.append(first_rotation)
            patterns.append(first_rotation[::-1])
            sec_rotation = self.rotation(first_rotation)
            patterns.append(sec_rotation)
            patterns.append(sec_rotation[::-1])
            third_rotation = self.rotation(sec_rotation)
            patterns.append(third_rotation)
            patterns.append(third_rotation[::-1])
        return patterns

    def match(self, map):
        for pattern in self.allpatterns(): # TODO include rotations and mirrior
            missmatch=False
            if (len(map)%len(pattern)==0 and len(map[0])%len(pattern[0])==0):
                blowuppattern=self.pattern_blow_up(pattern,int(len(map)/len(pattern)))
                for row_map, row_pattern in zip(map,blowuppattern):
                    for cell_map, cell_pattern in zip(row_map, row_pattern):
                        if (cell_map==1 and cell_pattern==0) or (cell_map!=1 and cell_pattern!=0):
                            pass
                        else:
                            missmatch=True
                            break
                    if missmatch:
                        break
                if not missmatch:
                    return blowuppattern

    def get_squares(self, map, pattern):
        squares=[[],[],[],[],[],[]]
        for row_map, row_pat in zip(map,pattern):
            rows=[[],[],[],[],[],[]]
            for cell_map, cell_pat in zip(row_map, row_pat):
                if cell_pat>0:
                    rows[cell_pat-1].append(cell_map)
            for row,map in zip(rows, squares):
                if row:
                    map.append(row)
        return squares

c=cube()
pattern=c.match(map)

squares_parsed=c.get_squares(map,pattern)

square_elm=[]

for s in squares_parsed:
    square_elm.append(squares(s))

# this part only fits for my input pattern
square_elm[0].set_right(square_elm[1], "r")
square_elm[0].set_down(square_elm[2], "u")
square_elm[0].set_left(square_elm[3], "u")
square_elm[0].set_up(square_elm[4], "u")

square_elm[1].set_right(square_elm[0], "r")
square_elm[1].set_down(square_elm[4], "l")
square_elm[1].set_left(square_elm[5], "r")
square_elm[1].set_up(square_elm[2], "r")



square_elm[2].set_right(square_elm[1], "u")
square_elm[2].set_down(square_elm[5], "u")
square_elm[2].set_left(square_elm[3], "r")
square_elm[2].set_up(square_elm[0], "d")


square_elm[3].set_right(square_elm[2], "l")
square_elm[3].set_down(square_elm[5], "r")
square_elm[3].set_left(square_elm[4], "r")
square_elm[3].set_up(square_elm[0], "l")


square_elm[4].set_right(square_elm[3], "l")
square_elm[4].set_down(square_elm[5], "d")
square_elm[4].set_left(square_elm[1], "u")
square_elm[4].set_up(square_elm[0], "u")


square_elm[5].set_right(square_elm[1], "l")
square_elm[5].set_down(square_elm[4], "d")
square_elm[5].set_left(square_elm[3], "d")
square_elm[5].set_up(square_elm[2], "d")

# TODO check the constraints





position=square_elm[0].map[0].index(0)
position=(square_elm[0],0,position,0,1)
for instruct in instructions:
    if isinstance(instruct , int):
        for _ in range(instruct):

            next_position = position[0].move(position[1],position[2],position[3],position[4])

            if next_position[0].map[next_position[1]][next_position[2]]==0:
                position = next_position

    else:
        if instruct=='R':
            if position[4]==1:
                position=(position[0], position[1],position[2],1,0)
            elif position[3]==1:
                position = (position[0], position[1],position[2], 0, -1)
            elif position[4] == -1:
                position = (position[0], position[1],position[2], -1, 0)
            elif position[3] == -1:
                position = (position[0], position[1],position[2], 0, 1)
        else:
            if position[4]==1:
                position=(position[0], position[1],position[2],-1,0)
            elif position[3]==1:
                position = (position[0], position[1],position[2], 0, 1)
            elif position[4] ==  -1:
                position = (position[0], position[1],position[2], 1, 0)
            elif position[3] == -1:
                position = (position[0], position[1],position[2], 0, -1)

if position[4]==1:
    third = 0
elif position[3]==1:
    third = 1
elif position[4]==-1:
    third = 2
else:
    third=3

print(square_elm.index(position[0]))
print(4+position[1],4+position[2])
print(1000*(4+position[1]+1)+4*(4+position[2]+1)+third)