#read lines of the file
with open("day01.txt", "rt") as myfile:
    digits = myfile.readlines()

calories=[] # init al list for the elves
cur=0   # init a counter to sum each elve
for d in digits:    #iterate over the lines
    if d.strip() == '': # if the line is empty store the elve and reste
        calories.append(cur)
        cur=0
    else:
        cur+=int(d.strip()) # add the line to counter
calories.sort(reverse=True) # sort descending
print(calories[0])  # 69528 print the first
print(calories[0]+calories[1]+calories[2]) # 206152 print the sum of the first three

