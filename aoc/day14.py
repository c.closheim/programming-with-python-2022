# function calculating all points between
def allbetween(pair_one, pair_two):
    resultset={pair_one,pair_two}
    if pair_one[0]==pair_two[0]:
        for i in range(min(pair_one[1],pair_two[1]),max(pair_one[1],pair_two[1])):
            resultset.add((pair_one[0],i))
    else:
        for i in range(min(pair_one[0],pair_two[0]),max(pair_one[0],pair_two[0])):
            resultset.add((i,pair_one[1]))
    return resultset

#read lines of the file
with open("day14.txt", "rt") as myfile:
   file = myfile.readlines()

#parse problem
stones=set() # use a set rep
for line in file:
    line=line.strip().split('->')
    points=[]
    for pair in line:
        pair=pair.split(',')
        points.append((int(pair[0]), int(pair[1]))) # add points to the list
    for left, right in zip(points, points[1:]): # iter over all pairs
        stone=allbetween(left,right) # add all between
        stones=stones.union(stone)

deepest_line = max(stones, key=lambda s: s[1])[1] # get the deepest line point

#part 1
all_sand_rest=False
restet_sand=set() # restet sand cornes
while not all_sand_rest: # iter while not all sand corns rested
    item = (500,0) # start point
    while item[1] <= deepest_line: # not under the deepest line
        blockset=stones.union(restet_sand)
        if (item[0],item[1]+1) in blockset: # check where the sand corn has to fall
            if (item[0]-1,item[1]+1) in blockset:
                if (item[0]+1,item[1]+1) in blockset:
                    restet_sand.add(item) # rest
                    break
                else:
                    item=(item[0]+1,item[1]+1)# right
            else:
                item = (item[0]-1,item[1]+1) # left
        else:
            item = (item[0],item[1]+1) # down
    if item[1] > deepest_line:
        break # break if free fall
print(len(restet_sand)) # 1078


#part 2
all_sand_rest=False
restet_sand=set()
count=0
while not(500,0) in restet_sand:
    item = (500,0) # init
    while True:
        blockset=stones.union(restet_sand)
        if (item[0],item[1]+1) in blockset or item[1]+1==deepest_line+2: # deepestline +2 infinite line
            if (item[0]-1,item[1]+1) in blockset or item[1]+1==deepest_line+2:
                if (item[0]+1,item[1]+1) in blockset or item[1]+1==deepest_line+2:
                    restet_sand.add((item[0]+0,item[1]+0))
                    break
                else:
                    item=(item[0]+1,item[1]+1)
            else:
                item = (item[0]-1,item[1]+1)
        else:
            item = (item[0],item[1]+1)
    drop_set=set()
    blockset = stones.union(restet_sand)
    for sand in restet_sand:
        if (sand[0], sand[1]-1) in blockset and (sand[0]+1, sand[1]-1)in blockset and (sand[0] - 1, sand[1]-1) in blockset:
            drop_set.add(sand) # remove sand corne cover by sand to make compersion faster
    count+=len(drop_set)
    restet_sand = restet_sand-drop_set

print(count+len(restet_sand)) # sum the removed items and the rested sand
