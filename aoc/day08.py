# function to rotated a matrix
def matrix_turn_left(matrix):
    return [[matrix[j][i] for j in reversed(range(len(matrix)))] for i in range(len(matrix[0]))]

with open("day08.txt", "rt") as myfile:
   file = myfile.readlines()

matrix=[[int(c) for c in line.strip()] for line in file] # build the matrix
vis_map=[[0 for c in line.strip()] for line in file] # a map to check the visability

#part 1
for _ in range(4): # check from all 4 sides
    for row in range(len(matrix)): # check all rows
        height_max=matrix[row][0] # start with the first tree as the highest
        vis_map[row][0]=1 # mark the edge as visible
        for col in range(1,len(matrix[row])): # leave the first out
            if matrix[row][col]>height_max: # check if the the tree is higher as the highest
                vis_map[row][col] = 1 # mark the vis map
                height_max=matrix[row][col]  # change the highest

    matrix=matrix_turn_left(matrix) # rotated the matrix and vis matrix
    vis_map = matrix_turn_left(vis_map)

count=sum([sum(row) for row in vis_map]) # sum the vis map up

print(count) # 1538


#part 2

scenic_score_map=[[0 for c in line.strip()] for line in file]
for row in range(len(matrix)):
    for col in range(len(matrix[row])):
        height = matrix[row][col]
        left, right, up, down=0,0,0,0
        for i in range(1,len(matrix[row])-col): # go through each 4 directions an increase the counter
            right+=1
            if matrix[row][col+i]>= height: # break if the tree is higher
                break
        for i in range(1,col+1):
            left+=1
            if matrix[row][col-i]>= height:
                break
        for i in range(1, len(matrix)-row):
            down += 1
            if matrix[row+i][col] >= height:
                break
        for i in range(1,row+1):
            up += 1
            if matrix[row - i][col] >= height:
                break
        scenic_score_map[row][col]=left*right*up*down # multiply each direction vis count

maximum=max([max(row) for row in scenic_score_map]) # find the maximum in the map

print(maximum) # 496125



