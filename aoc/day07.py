class File: # a class to represent a file with name and size
    def __init__(self, name, size):
        self.name=name
        self.size =size

class Dir: # a class to represent a dir with name parent, subdirs or files
    def __init__(self, name, parent):
        self.name=name
        self.childfile=[]
        self.childdir = []
        self.parent=parent

    def addFile(self, other): #add a file to dir
        self.childfile.append(other)

    def addDir(self, other): # add a dir to this dir
        self.childdir.append(other)

    def get_root(self): # return the root dir
        if self.parent==None:
            return self
        else:
            return self.parent.get_root()

    def get_size(self): # get the size of directory recursively
        return sum(cf.size for cf in self.childfile)+sum(cd.get_size() for cd in self.childdir)

    def get_size_list(self): # return a list of all sizes
        list=[self.get_size()]
        for cd in self.childdir:
            list.extend(cd.get_size_list() )
        return list

#build file system representation
with open("day07.txt", "rt") as myfile:
   file = myfile.readlines()

#root dir is the system
dir = Dir('System',None)
for line in file:
    line=line.strip()
    if '$ ls' in line or 'dir ' in line:
        pass # ignore the ls or dirs
    elif '$ cd ' in line:
        name = line.replace('$ cd ','') # we change the dir
        if name =='..':
            dir =dir.parent # go a dir up
        else:
            newdir=Dir(name,dir)  # go a new dir down
            dir.addDir(newdir)
            dir=newdir  # enter this dir
    else:   # it must be a file
        file=line.split(' ')
        dir.addFile(File(file[1], int(file[0])))    # add this file to the current dir
dir=dir.get_root().childdir[0]
#return the root dir

#part 1
print(sum(i for i in dir.get_size_list() if i <=100000)) # 1886043 filter the dirs and subs with less than 100000 and sum them up

#part 2
current_best=dir.get_size()
need=30000000-70000000+current_best # cal the needed space
for i in dir.get_size_list():
    if i-need>=0 and i-need < current_best-need: # choose the best item  need to free more space than needed and need to be smaller
        current_best=i # change the current best
print(current_best) # 3842121