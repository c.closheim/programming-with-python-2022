import copy

# rep of a search graph
class Node:
    def __init__(self, height, row, col):
        self.height=height
        self.row=row
        self.col = col
        self.exp = False

    def set_explored(self):
        self.exp=True

    def explored(self):
        return self.exp

    def set_queue_parent(self, node):
        self.parent=node

    def adjacent(self, node):
        rowadjacent=abs(node.row-self.row)==1 and node.col-self.col==0
        coladjacent=abs(node.col-self.col)==1 and node.row-self.row==0
        return rowadjacent or coladjacent

    def __eq__(self, other):
        return other!=None and self.row==other.row and self.col==other.col

class Edge:
    def __init__(self, node_one, node_two):
        self.node_one=node_one
        self.node_two=node_two

class Graph:
    def __init__(self):
        self.nodes=[]
        self.edges=[]

    def add_node(self,node:Node):
        self.nodes.append(node)

    def set_start_node(self, node:Node):
        self.start=node

    def set_goal_node(self, node: Node):
        self.goal = node

    def get_node(self,row, col):
        for node in self.nodes:
            if node.row==row and node.col==col:
                return node

    def add_edge(self, edge:Edge):
        self.edges.append(edge)

    def get_neighbors(self, node:Node):
        results=[]
        for edge in self.edges:
            if edge.node_one==node:
                results.append(edge.node_two)
        return results

#read lines of the file
with open("day12.txt", "rt") as myfile:
   file = myfile.readlines()

# parse problem
matrix=[] # convert the map into a int matrix
for row, line in enumerate(file):
    line_list=[]
    line=line.strip()
    for col, char in enumerate(line):
        if char == 'S': # remember the start
            line_list.append(ord('a')-97)
            start_coord=(row, col)
        elif char=='E': # remember the goal
            line_list.append(ord('z')-97)
            goal_coord = (row, col)
        else:
            line_list.append(ord(char)-97)
    matrix.append(line_list)
#part 1
graph=Graph() # build a graph from the matrix and append the nodes
for row, line in enumerate(matrix):
    for col, height in enumerate(line):
        node= Node(height, row,col)
        graph.add_node(node)
        if start_coord==(row,col):
            graph.set_start_node(node)
        if goal_coord == (row,col):
            graph.set_goal_node(node)
# now add the edges by checking the adjacent criteria
for nodes_one in graph.nodes:
    for nodes_two in graph.nodes:
        if nodes_one.adjacent(nodes_two) and nodes_two.height<=nodes_one.height+1:
            graph.add_edge(Edge(nodes_one,nodes_two))



start=graph.start
goal =graph.goal
start.set_explored() # marke explored nodes
queue=[start] # search queue
foundgoal=False

while len(queue)>0 and not foundgoal: #iterate while not in goal or nodes left
    node = queue.pop(0)
    if node==goal: # if found break
        foundgoal = True
        finalnode=node
        break
    next = graph.get_neighbors(node) # get all successors
    for successor in next:
        if not successor.explored():    # only use if not explored
            successor.set_explored()
            successor.set_queue_parent(node)
            queue.append(successor)

# go beakward from goal to find shortest path to start
count=0
while finalnode!= start:
    finalnode=finalnode.parent
    count+=1

print(count) # 391

#part 2
#idea search beakward from goal to a possible start
graph=Graph()
goals=[]
for row, line in enumerate(matrix):
    for col, height in enumerate(line):
        node= Node(height, row,col)
        graph.add_node(node)
        if height==0:
            goals.append(node)
        if goal_coord == (row,col):
            graph.set_start_node(node)
graph.set_goal_node(goals)
# make all level a nodes to goal nodes

#add adjacent edges but inverse
for nodes_one in graph.nodes:
    for nodes_two in graph.nodes:
        if nodes_one.adjacent(nodes_two) and nodes_two.height>=nodes_one.height-1:
            graph.add_edge(Edge(nodes_one,nodes_two))

start=graph.start
goal =graph.goal
start.set_explored()
queue=[start]
foundgoal=False

# same surch as before
while len(queue)>0 and not foundgoal:
    node = queue.pop(0)
    if node in goal:
        foundgoal = True
        finalnode=node
        break
    next = graph.get_neighbors(node)
    for successor in next:
        if not successor.explored():
            successor.set_explored()
            successor.set_queue_parent(node)
            queue.append(successor)

count=0
while finalnode!= start:
    finalnode=finalnode.parent
    count+=1

print(count) # 386