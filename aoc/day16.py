import copy
import itertools
import time

# build a graph struct
# use nodes
class Node:
    def __init__(self, name, rate,neighbors):
        self.name=name
        self.rate = rate
        self.open = False
        self.neighbors =neighbors

    def open_it(self):
        self.open =True

    def is_open(self):
        return self.open

    # for a given nodelist transforme a namelist to a nodelist
    def replace_neighbors(self, nodes):
        new_neighbors=set()
        for n in nodes:
            if n.name in self.neighbors:
                new_neighbors.add(n)
        self.neighbors=new_neighbors

    # build a tree struct from graph
    def tree_struct(self, nodes):
        self.tree_deep=[[self]]
        cover=set()
        cover.add(self)
        target=set(nodes)
        neighbors=self.neighbors
        while cover!=target:
            nextneighbors=set()
            deep=[]
            for neighbor in neighbors:
                cover.add(neighbor)
                deep.append(neighbor)
                nextneighbors=nextneighbors.union(neighbor.neighbors)
            self.tree_deep.append(deep)
            nextneighbors.difference_update(cover)
            neighbors =nextneighbors

    # cal the dit between two nodes
    def distance(self, node):
        for i, nodes in enumerate(self.tree_deep):
            if node in nodes:
                return i

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return other!=None and self.name==other.name


# read the file
with open("day16.txt", "rt") as myfile:
   file = myfile.readlines()

#build the graph by parsing
nodes=set()
allrates=[]
for line in file:
    line = line.strip().replace('Valve ','').replace('has flow rate=','').replace('; tunnels lead to valves','').replace('; tunnel leads to valve','').replace(',','').split(' ')
    name=line[0]
    rate=int(line[1])
    allrates.append(rate)
    neighbors=set()
    for i in range(2, len(line)):
        neighbors.add(line[i])
    node=Node(name,rate, neighbors)
    nodes.add(node)
    if(name=='AA'):
        startnode=node

# replace the neighbors with nodes
for node in nodes:
    node.replace_neighbors(nodes)
# call the tree  to call the distance
not_null_node=[]
for node in nodes:
    node.tree_struct(nodes)
    if node.rate>0:
        not_null_node.append(node)

not_null_node.sort(reverse=True, key=lambda a:a.rate)

# bound part

# call the theo best possible value as bound
def best_possible(remain_nodes, step):
    value=0
    for i, val in enumerate(remain_nodes):
        value+=val.rate*(step-1-i)
    return value

#branch part

#part 1

#find a DP solution here
class searchtree:
    def __init__(self,node, nodes, current_reward, steps):
        self.node=node
        self.nodes=nodes
        self.reward=current_reward
        self.steps=steps

    def build_childs(self, limit): # expand the node using the limit
        self.childs=[]
        if len(self.nodes)>0:
            for node in self.nodes:
                remain_nodes=copy.copy(self.nodes)
                remain_nodes.remove(node)
                steps =self.steps- self.node.distance(node) - 1 # steps so far
                if steps>=0:
                    reward =self.reward+ steps * node.rate # rewards
                    best_practice = best_possible(remain_nodes, step=steps) + reward # the best possible solution so far
                    if best_practice >= limit/1.4: #only if best practice is greater than limit expand
                        child=searchtree(node,remain_nodes,reward, steps)
                        child.build_childs(limit)
                        best = child.best_reward() # return best reward
                        self.childs.append(child)
                        limit=max(best,limit)

    def best_reward(self):
        if len(self.childs)>0:
            best=0
            for child in self.childs:
                best=max(best, child.best_reward()) # cal the best reward recursively
            return best
        return self.reward


tree=searchtree(startnode,not_null_node,0,30) # search tree with start node
tree.build_childs(0)
single_reward=tree.best_reward()
print(single_reward)
#1880

#part2
splits = [{tuple(c),tuple([x for x in not_null_node if x not in list(c)]) }for i in range(len(not_null_node)) for c in itertools.combinations(not_null_node, i+1)]
# check for each splitting combination
splitscopy=[]
for s in splits:
    if s not in splitscopy:
        splitscopy.append(s)
splits=splitscopy
max_reward=0
for i,split in enumerate(splits): # check each splits
    start=time.time()
    split=list(split)
    firstlist=list(split[0])
    firstlist.sort(reverse=True, key=lambda a:a.rate)
    tree = searchtree(startnode,firstlist , 0, 26)
    tree.build_childs(0)
    seclist = list(split[1])
    seclist.sort(reverse=True, key=lambda a: a.rate)
    sectree= searchtree(startnode, seclist, 0, 26)
    sectree.build_childs(0)
    max_reward=max(max_reward,tree.best_reward()+sectree.best_reward())
    end=time.time()
print(max_reward)
#2520




