#read lines of the file
with open("day09.txt", "rt") as myfile:
   file = myfile.readlines()

# parse commands
commands=[] # the command list
for line in file:
    com = line.strip().split(' ') # parse the commands to a vector and a number
    if com[0]=='R':
        commands.append(((0,1),int(com[1])))
    elif com[0]=='L':
        commands.append(((0,-1),int(com[1])))
    elif com[0]=='U':
        commands.append(((1,0),int(com[1])))
    else:
        commands.append(((-1, 0), int(com[1]))) # must be down

#part 1
h_pos=(0,0) # start position of the head
t_pos=(0,0) # start position of the tail
t_pos_set={t_pos}
for (direction, times) in commands:
    for t in range(times):
        h_pos=(h_pos[0]+direction[0],h_pos[1]+direction[1])
        if abs(h_pos[0]-t_pos[0])>1 or abs(h_pos[1]-t_pos[1])>1: # if distance is greater 1 in one direction
            if h_pos[1] > t_pos[1]: # build the new point if y pos is greater
                y = t_pos[1] + 1
            else:
                y = t_pos[1] - 1
            if h_pos[0] > t_pos[0]:# build the new point if x pos is greater
                x = t_pos[0] + 1
            else:
                x = t_pos[0] - 1

            if h_pos[0]==t_pos[0]: # if x is equal only change y
                t_pos=(t_pos[0],y)
            elif h_pos[1]==t_pos[1]: # if y is equal only change x
               t_pos=(x,t_pos[1])
            else:   # if both change change both
                t_pos = (x,y)
        t_pos_set.add(t_pos) # add to the set

print(len(t_pos_set)) # 6030



# part 2
rope_pos=[(0,0) for _ in range(10)] # build the rop start position
rope_end_pos_set={rope_pos[9]} # use the last  position as tail end
for (direction, times) in commands:
    for t in range(times):
        rope_pos[0]=(rope_pos[0][0]+direction[0],rope_pos[0][1]+direction[1])
        for i in range(0,9): # iterate over each tail part
            if abs(rope_pos[i][0]-rope_pos[i+1][0])>1 or abs(rope_pos[i][1]-rope_pos[i+1][1])>1:
                if rope_pos[i][1] > rope_pos[i+1][1]:
                    y = rope_pos[i+1][1] + 1
                else:
                    y = rope_pos[i+1][1] - 1
                if rope_pos[i][0] > rope_pos[i+1][0]:
                    x = rope_pos[i+1][0] + 1
                else:
                    x = rope_pos[i+1][0] - 1

                if rope_pos[i][0]==rope_pos[i+1][0]:
                    rope_pos[i+1]=(rope_pos[i+1][0],y)
                elif rope_pos[i][1]==rope_pos[i+1][1]:
                   rope_pos[i+1]=(x,rope_pos[i+1][1])
                else:
                    rope_pos[i+1] = (x,y)
        rope_end_pos_set.add(rope_pos[9])# add the tail end to the set

print(len(rope_end_pos_set)) # 2545