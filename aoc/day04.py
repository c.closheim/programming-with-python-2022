#read lines of the file
with open("day04.txt", "rt") as myfile:
    id_pairs = myfile.readlines()

id_pairs_list=[]
for id_pair in id_pairs:
    id_pair = id_pair.strip().split(',') # split at ,
    ids_0 = id_pair[0].split('-') # split at -
    ids_1 = id_pair[1].split('-')
    id_pairs_list.append(((int(ids_0[0]),int(ids_0[1])),(int(ids_1[0]),int(ids_1[1])))) # store the splits in a list


#part 1
count=0
for(a1,a2),(b1,b2) in id_pairs_list:
    if (a1 >= b1 and a2 <= b2) or (a1 <= b1 and a2 >= b2): # use the rule to increase the counter if overlapping totaly
        count+=1
print(count) # 584

#part 2
count=0
for(a1,a2),(b1,b2) in id_pairs_list:
    if not (a1>b2 or b1>a2): # use the rule to increase the counter overlapping
        count+=1

print(count) # 933


