# read the file
with open("day25.txt", "rt") as myfile:
   file = myfile.readlines()

# convert a snafu sign easy to a decimal int
def snafuSignToDecimal(snafu):
    if snafu=='2':
        return 2
    if snafu =='1':
        return 1
    if snafu =='0':
        return 0
    if snafu =='-':
        return -1
    if snafu =='=':
        return -2

# convert a complet snafu number to decimal
def snafuToDecimal(snafu):
    value = 0
    for i, sign in enumerate(snafu[::-1]):
        value += (5 ** i * snafuSignToDecimal(sign)) # convert each sign 5 to power of pos  and sum up
    return value

# decimal to snafu
def decimalToSnafu(decimal):
    value=[]
    tra=decimal
    if tra == 0: # simple case return 0
        return '0'
    remember=0 # take with in next step
    while tra!=0 or remember!=0:
        test=tra%5 #modulo
        add=test+remember # and remember
        if add<3: # if add <3 a simple sign can used
            value.append(str(add))
            remember=0
        else:
            if add ==3: # take with if >=3
                remember=1
                value.append('=')
            elif add ==4:
                value.append('-')
                remember=1
            elif add==5:
                value.append('0')
                remember = 1
        if tra <5:
            tra=0
        tra=tra//5 # divide by 5 and take the rest in the next round
    value.reverse() # reverse the val
    result= ''.join([str(val) for val in value]) # build the string rep
    return result

sum=0
for line in file:
    sum+= snafuToDecimal(line.strip()) # sum up



print(decimalToSnafu(sum)) # convert back to snafu
