import copy

class monkey:
    def __init__(self, inputfile): # parse the monkey
        self.inspectioncount=0
        self.items = [int(i) for i in inputfile[1].strip().replace('Starting items:', '').split(', ')]
        func = inputfile[2].strip().replace('Operation: new =', '').replace('old', 'x')
        self.operation = lambda x: eval(func)
        self.divide=int(inputfile[3].strip().replace('Test: divisible by ', ''))
        if_true=int(inputfile[4].strip().replace('If true: throw to monkey', ''))
        if_false=int(inputfile[5].strip().replace('If false: throw to monkey', ''))
        self.test = lambda x: if_true if x % self.divide==0 else if_false

    def hold_items(self):
        return len(self.items)>0

    def inspection_worry(self):
        self.inspectioncount+=1
        return self.operation(self.items[0])

    def through(self, worry_level):
        monkey=self.test(worry_level)
        self.items.pop(0)
        return monkey, worry_level

    def catch(self, item):
        self.items.append(item)

#read lines of the file
with open("day11.txt", "rt") as myfile:
   file = myfile.readlines()

#parse monkeys
monkeys=[]
while len(file)>0:
    monkeystring = file[0:6] # split the file an build the monkey list
    monkeys.append(monkey(monkeystring))
    file=file[6:]
    if len(file)>0 and file[0].strip()=='':
        file = file[1:]

monkeys_copy = copy.deepcopy(monkeys)
#part 1
for _ in range(20): # iterate for 20 rounds
    for monkey in monkeys:
        while monkey.hold_items(): # execute the throwing cycle
            worry=monkey.inspection_worry()
            worry=int(worry/3)
            new_monkey, item =monkey.through(worry)
            monkeys[new_monkey].catch(item)

inspectioncounts=[monkey.inspectioncount for monkey in monkeys] # the inspectioncounts for each monkey
inspectioncounts.sort() # sort them asc
level_monkey_business = inspectioncounts[-1]*inspectioncounts[-2] # multiply th top most
print(level_monkey_business) # 88208



#part 2
monkeys = copy.deepcopy(monkeys_copy)
modulo=1 # claculate the modulo operator
for monkey in monkeys:
    modulo*=monkey.divide # the modulo is the divider by each monkey
for _ in range(10000): # iterate 10000 times
    for monkey in monkeys:
        while monkey.hold_items():
            worry=monkey.inspection_worry()
            worry=worry% modulo
            new_monkey, item =monkey.through(worry)
            monkeys[new_monkey].catch(item)

inspectioncounts=[monkey.inspectioncount for monkey in monkeys]
inspectioncounts.sort()
level_monkey_business = inspectioncounts[-1]*inspectioncounts[-2]
print(level_monkey_business) # 21115867968