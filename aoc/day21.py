import copy

# build a monkey class
class monkey:
    def __init__(self, name):
        self.name=name
        self.value=None
        self.value1 = None
        self.value2 = None

    def set_value(self, value):
        self.value=value

    def evaluate_backward(self, expected):
        if self.value!=None and self.value!=expected:
            return False
        else:
            if self.value1!=None:
                if self.func=='+':
                    value=expected-self.value1
                elif self.func=='-':
                    value=self.value1-expected
                elif self.func=='*':
                    value=expected/self.value1
                elif self.func=='/':
                    value=self.value1/expected
                elif self.func=='=':
                    value=self.value1
                return self.monkey2, value
            elif self.value2!=None:
                if self.func=='+':
                    value=expected-self.value2
                elif self.func=='-':
                    value=self.value2+expected
                elif self.func=='*':
                    value=expected/self.value2
                elif self.func=='/':
                    value=self.value2*expected
                elif self.func=='=':
                    value=self.value2
                return self.monkey1, value

    def evaluate(self, monkeylist):
        if self.value!=None:
            return True

        elif self.value1!=None:
            for monkey in monkeylist:
                if monkey.name == self.monkey2:
                    self.value2 = monkey.value
                    break
            if self.value2!=None:
                self.set_value(self.formula(self.value1, self.value2))
                return True
            return False
        elif self.value2!=None:
            for monkey in monkeylist:
                if monkey.name == self.monkey1:
                    self.value1 = monkey.value
                    break
            if self.value1!=None:
                self.set_value(self.formula(self.value1, self.value2))
                return True
            return False

        count = 0
        for monkey in monkeylist:
            if monkey.name == self.monkey1:
                self.value1 = monkey.value
                count += 1
            if monkey.name == self.monkey2:
                self.value2 = monkey.value
                count += 1
            if count >= 2:
                break
        if self.value1 != None and self.value2 != None:
            self.set_value(self.formula( self.value1 , self.value2))
            return True
        return False

    def set_mul_expression(self, monkey1, monkey2):
       self.monkey1=monkey1
       self.monkey2=monkey2
       self.formula=lambda value1, value2:value1*value2
       self.func='*'

    def set_min_expression(self, monkey1, monkey2):
        self.monkey1 = monkey1
        self.monkey2 = monkey2
        self.formula = lambda value1, value2: value1 - value2
        self.func='-'

    def set_add_expression(self, monkey1, monkey2):
        self.monkey1 = monkey1
        self.monkey2 = monkey2
        self.formula = lambda value1, value2: value1 + value2
        self.func = '+'

    def set_div_expression(self, monkey1, monkey2):
        self.monkey1 = monkey1
        self.monkey2 = monkey2
        self.formula = lambda value1, value2: value1 / value2
        self.func = '/'

    def set_equal_expression(self, monkey1, monkey2):
        self.monkey1 = monkey1
        self.monkey2 = monkey2
        self.formula = lambda value1, value2: value1 == value2
        self.func = '='

#parse prolem
with open("day21.txt", "rt") as myfile:
   file = myfile.readlines()
expression=[]

#build the monkey list
for line in file:
    line=line.strip().split(':')
    mon =monkey(line[0])
    if mon.name=='root':
        root=mon
    line[1]=line[1].replace(' ','')
    if line[1].isnumeric():
        mon.value=int(line[1])
    else:
        if '+' in line[1]:
            pair=line[1].split('+')
            mon.set_add_expression(pair[0],pair[1])
        elif '-' in line[1]:
            pair=line[1].split('-')
            mon.set_min_expression(pair[0],pair[1])
        elif '*' in line[1]:
            pair=line[1].split('*')
            mon.set_mul_expression(pair[0],pair[1])
        elif '/' in line[1]:
            pair=line[1].split('/')
            mon.set_div_expression(pair[0],pair[1])

    expression.append(mon)

#part 1
expression_copy = copy.deepcopy(expression)
alltrue=False
while not alltrue: # iterate while not all monkey eval
    alltrue=True
    for monkey in expression:
        if not monkey.evaluate(expression):
            alltrue=False

print(root.value) # out the root val

#part2
expression = copy.deepcopy(expression_copy)
alltrue=False
for monkey in expression:
    if monkey.name=='root':
        monkey.set_equal_expression(root.monkey1, root.monkey2)
    if monkey.name=='humn':
        humn=monkey
expression.remove(humn)
evaluatelist=[False for _ in expression]
new_evaluatelist=[True for _ in expression]
while not evaluatelist==new_evaluatelist:
    new_evaluatelist=copy.copy(evaluatelist)
    for i,monkey in enumerate(expression):
        evaluatelist[i]=monkey.evaluate(expression)

expression_copy = copy.deepcopy(expression)
for monkey in expression:
    if monkey.name == 'root':
        root=monkey
        break
nextmonkey, value=root.evaluate_backward(True) # eval back

while nextmonkey!="humn":
    for monkey in expression:
        if monkey.name == nextmonkey:
            nextmonkey = monkey
            break
    nextmonkey, value=nextmonkey.evaluate_backward(value)

print(value)

