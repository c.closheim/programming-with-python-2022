#read lines of the file
with open("day10.txt", "rt") as myfile:
   file = myfile.readlines()

#fill register history
register=[1] # register start with 1
for line in file:
    line = line.strip().split(' ')
    register.append(register[-1])  # one cycle is needed always
    if line[0]=='noop':
        pass    # skip
    else:
        register.append(register[-1]+int(line[1])) # if not noop add to register, according to cycles needed

#part 1
sum=0
for i in range(6):
    sum+=(20+40*i)*register[20+40*i-1] # sum up according to cycles

print(sum) # 17180


#part 2

pos=register.pop(0) # get first register entry
row =''
for cycle, entry in enumerate(register): # for each cycle and entry in register
    if cycle%40 == pos-1 or cycle%40 == pos or cycle%40 == pos+1: # modulo in relation to pos
        row+='#'
    else:
        row += '.'
    pos=entry
    if len(row)%40==0:
        print(row)
        row=''


###..####.#..#.###..###..#....#..#.###..
#..#.#....#..#.#..#.#..#.#....#..#.#..#.
#..#.###..####.#..#.#..#.#....#..#.###..
###..#....#..#.###..###..#....#..#.#..#.
#.#..#....#..#.#....#.#..#....#..#.#..#.
#..#.####.#..#.#....#..#.####..##..###..
