import json

# define a compare function
def compare(left, right):
    if type(left) is int and type(right) is int: # if both sides are int
        if left < right: # use a simple compare
            return True
        elif left>right:
            return False
        else:
            return None # use none if equal
    elif type(left) is list and type(right) is list: # if both sides are lists
        for l,r in zip(left, right): # iterate over each item pair
            check = compare(l,r) # compare recursively
            if check != None: # if not equal return the check
                return check
        if len(left)<len(right): # if equal rcompare the len
            return True
        elif len(left)>len(right):
            return False
        else:
            return None # if len equal return None
    elif type(left) is int and type(right) is list: # convert one side to list and recursive
        return compare([left],right)
    else:
        return compare(left, [right])

#parse problem
with open("day13.txt", "rt") as myfile:
   file = myfile.readlines()

pairs=[] # all pairs
pair=[] # the init pair
for line in file:
    line=line.strip()
    if line=='': # if line is empty add the pair list and reset the init pair
        pairs.append(pair)
        pair=[]
    else:
        line=json.loads(line) # use json to import the stacked lists
        pair.append(line) # add the json parsed list
pairs.append(pair) # add the last line


#part 1
comp=0
for i, pair in enumerate(pairs): # iter over all pairs
    left = pair[0] # get left
    right = pair[1] # get right
    com=compare(left,right) # compare the lists
    if com:
        comp+=i+1 # add the index (of by 1 error)
print(comp) # 5555

# part 2
a =[[2]]
b= [[6]]
all_pack=[a,b]
for pair in pairs: # iter over the pairs
    for part in pair: # for bot lists
        inserted=False
        for i in range(len(all_pack)): # for the orderd list
            if compare(part, all_pack[i]): # check if true insert and break
                all_pack.insert(i, part)
                inserted = True
                break
        if not inserted: # if not inserted append at the end
            all_pack.append(part)

print((all_pack.index(a)+1)*(all_pack.index(b)+1)) # 22852 find the index of bot list and return product (of by 1 error)

