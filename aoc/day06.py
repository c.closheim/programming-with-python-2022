import copy

#read lines of the file
with open("day06.txt", "rt") as myfile:
   file = myfile.read()


inp = list(file.strip()) # parse the input to list
inp.reverse() # reverse the list

#part 1
input = copy.deepcopy(inp)
for i in range(len(input)-3): # we need to exclude the last 4
    if len(set(input[-4:]))==4: # check if the last 4 elements are different
        c= i+4 # add 4 to the count since we skip 4
        break
    else:
        input.pop() # remove the last entry
print(c) # 1538

#part 2
input = copy.deepcopy(inp) # copy to reuse the input
for i in range(len(input)-13):
    if len(set(input[-14:]))==14: #check the last 14 are different
        c= i+14 # add 14 to the count since we skip 14
        break
    else:
        input.pop() # remove the last entry
print(c) # 2315
