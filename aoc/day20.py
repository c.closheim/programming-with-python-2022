
# a ring list class element
class ring_element:
    def __init__(self, number):
        self.number=number

    # set the neighbors
    def set_right(self, element):
        self.right=element
    def set_left(self, element):
        self.left=element

    # change position right or left
    def change_right(self):
        right=self.right
        self.right=right.right
        self.right.left=self
        right.right=self
        right.left=self.left
        self.left.right=right
        self.left=right

    def change_left(self):
        left = self.left
        self.left = left.left
        self.left.right = self
        left.left = self
        left.right = self.right
        self.right.left = left
        self.right = left


# read in the list
with open("day20.txt", "rt") as myfile:
   file = myfile.readlines()

#part 1
list_original=[] # ring elements
for row in file:
    list_original.append( ring_element( int(row.strip()))) # convert each to a ring element
for i in range(len(list_original)):
    element=list_original[i]
    if element.number==0:
        start=element   # 0 is the start
    element.left=list_original[i-1] # set right and left elements
    element.right= list_original[(i +1)%len(list_original) ]



for element in list_original: # iterate over each elem and execute the changes
    number=element.number
    if number>0:
        for _ in range(number%(len(list_original)-1)):
            element.change_right()
    elif number<0:
        for _ in range(abs(number)%(len(list_original)-1)): # leave the modulo to save time
            element.change_left()

res=[]
for _ in range(3):
    for _ in range(1000):
        start = start.right
    res.append(start.number) # 3 times 1000 times to the right
print(sum(res)) # 3700

#part 2

list_original=[]
for row in file:
    list_original.append(  ring_element( 811589153*int(row.strip()))) # mulitply each element
for i in range(len(list_original)):
    element=list_original[i]
    if element.number==0:
        start=element
    element.left=list_original[i-1]
    element.right= list_original[(i +1)%len(list_original) ]

# iterate 10 times the howl list
for _ in range(10):
    for element in list_original:
        number=element.number
        if number>0:
            for _ in range(number%(len(list_original)-1)):
                element.change_right()
        elif number<0:
            for _ in range(abs(number)%(len(list_original)-1)):
                element.change_left()

res=[] # cal the result
for _ in range(3):
    for _ in range(1000):
        start = start.right
    res.append(start.number)
print(sum(res)) # 10626948369382