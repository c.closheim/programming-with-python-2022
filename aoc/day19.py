import copy

with open("day19.txt", "rt") as myfile:
   file = myfile.readlines()

class Minearlstore:
    def __init__(self, ore,clay,obsidian,geode):
        self.ore=ore
        self.clay=clay
        self.obsidian = obsidian
        self.geode=geode
    def add(self, store):
        self.ore+=store.ore
        self.clay +=store.clay
        self.obsidian +=store.obsidian
        self.geode +=store.geode
    def sub(self, store):
        self.ore-=store.ore
        self.clay -=store.clay
        self.obsidian -=store.obsidian
        self.geode -=store.geode
    def contains_enough(self, store):
        return self.ore>=store.ore and self.clay >=store.clay and self.obsidian >=store.obsidian and self.geode >=store.geode

class blueprint:
    def __init__(self,number, orerobcost:Minearlstore, clayrobcost:Minearlstore, obsrobcost:Minearlstore, georobcost:Minearlstore):
        self.number=number
        self.orerobcost=orerobcost
        self.clayrobcost=clayrobcost
        self.obsrobcost=obsrobcost
        self.georobcost=georobcost

class robotarmada:
    def __init__(self, ore, clay, obs, geo):
        self.ore=ore
        self.clay=clay
        self.obs=obs
        self.geo=geo
    def step(self):
        return Minearlstore(self.ore,self.clay,self.obs,self.geo)
    def add(self,ore, clay, obs, geo):
        self.ore += ore
        self.clay += clay
        self.obs += obs
        self.geo += geo

class factory:
    def __init__(self, blueprint:blueprint):
        self.blueprint=blueprint
    def step(self, minearlstore:Minearlstore, robotarmada:robotarmada):
        while True:
            if minearlstore.contains_enough(self.blueprint.georobcost):
                minearlstore.sub(self.blueprint.georobcost)
                robotarmada.add(0,0,0,1)
            elif minearlstore.contains_enough(self.blueprint.obsrobcost):
                minearlstore.sub(self.blueprint.obsrobcost)
                robotarmada.add(0,0,1,0)
            elif minearlstore.contains_enough(self.blueprint.clayrobcost):
                minearlstore.sub(self.blueprint.clayrobcost)
                robotarmada.add(0,1,0,0)
            elif minearlstore.contains_enough(self.blueprint.orerobcost):
                minearlstore.sub(self.blueprint.orerobcost)
                robotarmada.add(1,0,0,0)
            else:
                break
        return robotarmada
    def possible_futures(self,minearlstore:Minearlstore, robotarmada:robotarmada):
        minearlstore_copy = copy.deepcopy(minearlstore)
        robotarmada_copy = copy.deepcopy(robotarmada)
        if minearlstore_copy.contains_enough(self.blueprint.georobcost):
            minearlstore_copy.sub(self.blueprint.georobcost)
            robotarmada_copy.add(0, 0, 0, 1)
            return [[minearlstore_copy,robotarmada_copy]]
        else:
            pos_fut = []
            if minearlstore_copy.contains_enough(self.blueprint.obsrobcost):
                minearlstore = copy.deepcopy(minearlstore_copy)
                robotarmada = copy.deepcopy(robotarmada_copy)
                minearlstore.sub(self.blueprint.obsrobcost)
                robotarmada.add(0, 0, 1, 0)
                pos_fut.append([minearlstore,robotarmada])

            if minearlstore_copy.contains_enough(self.blueprint.clayrobcost):
                minearlstore = copy.deepcopy(minearlstore_copy)
                robotarmada = copy.deepcopy(robotarmada_copy)
                minearlstore.sub(self.blueprint.clayrobcost)
                robotarmada.add(0, 1, 0, 0)
                pos_fut.append([minearlstore, robotarmada])

            if minearlstore_copy.contains_enough(self.blueprint.orerobcost):
                minearlstore = copy.deepcopy(minearlstore_copy)
                robotarmada = copy.deepcopy(robotarmada_copy)
                minearlstore.sub(self.blueprint.orerobcost)
                robotarmada.add(1, 0, 0, 0)
                pos_fut.append([minearlstore, robotarmada])
            if len(pos_fut)<3:
                pos_fut.append([minearlstore_copy,robotarmada_copy])
            return pos_fut



blueprints=[]
for line in file:
    line=line.strip().split(':')
    number=int(line[0].replace('Blueprint ',''))
    costs=line[1].split('.')
    orecost=Minearlstore(int(costs[0].replace('Each ore robot costs ','').replace(' ore','')),0,0,0)
    claycost=Minearlstore(int(costs[1].replace('Each clay robot costs ','').replace(' ore','')),0,0,0)
    obscost= costs[2].replace('Each obsidian robot costs ', '').replace(' ore and', '').replace(' clay', '').split(' ')
    obscost.remove('')
    obscost=Minearlstore(int(obscost[0]),int(obscost[1]),0,0)
    geocost = costs[3].replace('Each geode robot costs ', '').replace(' ore and', '').replace(' obsidian', '').split(' ')
    geocost.remove('')
    geocost=Minearlstore(int(geocost[0]),0,int(geocost[1]),0)
    blueprints.append(blueprint(number,orecost,claycost,obscost,geocost))

quality=0
for bluenumber,blue in enumerate(blueprints):
    print(bluenumber)
    store=Minearlstore(0,0,0,0)
    robs=robotarmada(1,0,0,0)
    fac=factory(blue)
    states=[[store,robs]]
    for i in range(24):

        future_states=[]
        best_geod = 0
        best_ops = 0
        best_geod_rob = 0
        best_ops_rob = 0
        for state in states:
            future=fac.possible_futures(state[0],state[1])
            getminerals=state[1].step()

            for fut in future:
                fut[0].add(getminerals)
                future_states.append(fut)
                best_geod=max(best_geod,fut[0].geode)
                best_ops = max(best_ops,fut[0].obsidian)
                best_geod_rob = max(best_geod_rob,fut[1].geo)
                best_ops_rob =max(best_ops_rob, fut[1].obs)
        new_states=[]
        for fut in future_states:
            if best_geod>0:
                if fut[0].geode== best_geod:
                    new_states.append(fut)
            else:
                if best_geod_rob>0:
                    if fut[1].geo == best_geod_rob:
                        new_states.append(fut)
                else:
                    if fut[0].obsidian == best_ops or fut[1].obs== best_ops_rob:
                        new_states.append(fut)
        states=new_states
    max_reward=0
    for state in states:
        max_reward = max(state[0].geode, max_reward)
    # print(max_reward)
    quality+=(bluenumber+1)*max_reward
print(quality)
# not working
# 786 to low