import re

# read the file
with open("day15.txt", "rt") as myfile:
   file = myfile.readlines()

# parse
problem=[]
for line in file:
    line =re.sub("[^0-9,:-]", "", line.strip()).split(':') # only use digits and -, and : and split at :
    sensordata=line[0].split(',')
    beacondata=line[1].split(',')
    sensor=(int(sensordata[0]), int(sensordata[1])) # sensor point
    beacon=(int(beacondata[0]), int(beacondata[1])) # nearest point
    problem.append((sensor,beacon)) # add to problem

#part 1
line_of_interest=2000000 # define line of interest
positions=set() # positions
beacons=set()
for sensor,beacon in problem:
    beacons.add(beacon)
    man_dis=abs(sensor[0]-beacon[0])+abs(sensor[1]-beacon[1]) # cal euc dist
    if (sensor[1] < line_of_interest and sensor[1]+man_dis>=line_of_interest) or  (sensor[1] > line_of_interest and sensor[1]-man_dis<=line_of_interest): # if target crossed
        positions.add(sensor[0]+abs(man_dis-abs(sensor[1]-line_of_interest))) # add mirrored data
        positions.add(sensor[0]-abs(man_dis - abs(sensor[1] - line_of_interest)))

count=0 # sum
for pos in (beacons): # sum over all beacons
    if pos[1]==line_of_interest:
        count+=1 # sum up
print(abs(min(positions))+abs(max(positions))-count+1) # 6078701

#part 2 helpstruct
class range_object: # build an object of ranges
    def __init__(self):
        self.parts=[]   # init a list of range parts

    def add(self, min, max):
        if len(self.parts)==0:
            self.parts.append((min,max)) # if no part exists
        else:
            for i in range(len(self.parts)): # check for overlapping
                mino,maxo=self.parts[i]
                if min==mino and max == maxo:
                    return  # if this range object exist leave it
                elif mino<=min and max<=maxo:
                    return # if range covered full by the other leave it
                elif min<=mino and maxo<=max:
                    self.parts[i] = (min, max)
                    return # if existing is full covered by the new take the new one
                elif mino<=min and min<=maxo:
                    self.parts[i] =(mino,max)
                    return # if the max is greater
                elif min<=mino and  mino<=max:
                    self.parts[i] = (min, maxo)
                    return # if the min is smaller
            self.parts.append((min, max)) # if the range object is full distinct to each other

    def merge(self): # try to summarize each range object
        for i in range(len(self.parts)-1):
            part=self.parts.pop(1)
            self.add(part[0],part[1])

    def gap_exists(self): # check if there is a gap in the range
        self.merge()
        return len(self.parts)>1


#part 2
maxarea=4000000
for line_of_interest in range(maxarea+1):
    ranges = range_object()
    for sensor,beacon in problem: # build the range objects
        man_dis=abs(sensor[0]-beacon[0])+abs(sensor[1]-beacon[1])
        if (sensor[1] <= line_of_interest and sensor[1]+man_dis>=line_of_interest) or  (sensor[1] >= line_of_interest and sensor[1]-man_dis<=line_of_interest):
            ranges.add(max(sensor[0]-abs(man_dis - abs(sensor[1] - line_of_interest)),0),min(sensor[0]+abs(man_dis-abs(sensor[1]-line_of_interest))+1,maxarea))
    ranges.merge() # merge all range objects
    if ranges.gap_exists(): # check for the gab
        print((ranges.parts[0][1])*4000000+line_of_interest) # 12567351400528
        break