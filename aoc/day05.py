from copy import deepcopy

#read lines of the file
with open("day05.txt", "rt") as myfile:
   input = myfile.readlines()

#read data
stacks = [] # the stacks
commands=[] # the commands
read_stacks=True
read_commands = False
for j,i in enumerate(input):
    i=i.replace('\n','') # can not use strip because the alignment is needed to identify stack
    if i!='' and i[1]=='1':
        read_stacks=False # the lines under the stacks
    if read_stacks:
        for k,s in enumerate(i): # append the stacks
            if len(stacks)>k:
                if s!=' ':
                    stacks[k].append(s)
            else:
                stacks.append([])
                if s!=' ':
                    stacks[k].append(s)

    if read_commands:
        c=i.split(' ')
        commands.append([int(c[1]),int(c[3]),int(c[5])]) # append the account numbers

    if i.strip()=='':
        read_commands=True # if the line is free change to commands
stacks=[stacks[i] for i in range(1,len(stacks),4)] # only use the stacks with alphas not brackets
for stack in stacks:
    stack.reverse() # reverse all stacks

#part one
stacks_one=deepcopy(stacks) # deepyopy the stacks to reuse it
for com in commands: # iterate over the commands
    for i in range(com[0]):
        stacks_one[com[2]-1].append(stacks_one[com[1]-1].pop())

res =''
for stack in stacks_one:
    res+=stack[-1] # read the last stack in

print(res) #ZSQVCCJLL

# part two
stacks_two=deepcopy(stacks) # deepcopy the original stacl
for com in commands:
    stacks_two[com[2]-1].extend(stacks_two[com[1]-1][-com[0]:])
    del stacks_two[com[1]-1][-com[0]:] # use with multiple

res =''
for stack in stacks_two:
    if len(stack)>0:
        res+=stack[-1] # read the last stack in

print(res) # QZFJRWHGS

