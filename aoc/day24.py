import copy

with open("day24.txt", "rt") as myfile:
   file = myfile.readlines()


class blizzard:
    def __init__(self, pos, direction, modulo):
        self.pos=pos
        self.direction=direction
        self.modulo=modulo
    def step(self):
        self.pos=((self.pos[0]+self.direction[0])%self.modulo[0],(self.pos[1]+self.direction[1])%self.modulo[1])



width=len(file[0].strip())-2
height=len(file)-2
for c, col in enumerate(file[0].strip()[1:-1]):
    if col=='.':
        start_pos =(-1,c)
for c, col in enumerate(file[-1].strip()[1:-1]):
    if col=='.':
        goal_pos =(height,c)

bliz=[]
for r, line in enumerate(file[1:-1]):
    for c, col in enumerate(line.strip()[1:-1]):
        if col == '<':
            bliz.append(blizzard((r,c),(0,-1),(height,width)))
        elif col == '^':
            bliz.append(blizzard((r, c), (-1, 0), (height, width)))
        elif col == '>':
            bliz.append(blizzard((r, c), (0, 1), (height, width)))
        elif col == 'v':
            bliz.append(blizzard((r, c), (1, 0), (height, width)))




map=[[0 for _ in range(width)] for _ in range(height)]

mapcopy=copy.deepcopy(map)
map=[[0 for _ in range(width)] for _ in range(height)]
for b in bliz:
    map[b.pos[0]][b.pos[1]]+=1
blizcycle=[map]


while True:
    map=[[0 for _ in range(width)] for _ in range(height)]
    for b in bliz:
        b.step()
        map[b.pos[0]][b.pos[1]]+=1
    if map==blizcycle[0]:
        break
    else:
        blizcycle.append(map)

def validposition(pos):
    return pos==start_pos or pos==goal_pos \
           or (pos[0]>=0 and pos[0]<height and pos[1]>=0 and pos[1]<width)
def positionsoptions(pos):
    return [(pos[0],pos[1]),(pos[0]+1,pos[1]),(pos[0]-1,pos[1]),(pos[0],pos[1]+1),(pos[0],pos[1]-1)]


#part 1


positions=[(start_pos,0)]
count=0
while True:
    next_positions=[]
    count += 1
    for pos, i in positions:
        for nextpos in positionsoptions(pos):

            if nextpos==goal_pos:
                break
            if validposition(nextpos) and (nextpos==start_pos or nextpos==goal_pos  or blizcycle[(i+1)%len(blizcycle)][nextpos[0]][nextpos[1]]==0):
                new_state=(nextpos,(i+1))
                if not new_state in next_positions:
                    next_positions.append(new_state)

        if nextpos == goal_pos:
            break
    if nextpos == goal_pos:
        break

    positions=next_positions

print(count)

#311


#part 2
positions=[(goal_pos,count%len(blizcycle))]
while True:
    next_positions=[]
    count += 1
    for pos, i in positions:
        for nextpos in positionsoptions(pos):
            if nextpos==start_pos:
                break
            if validposition(nextpos) and (nextpos==start_pos or nextpos==goal_pos  or blizcycle[(i+1)%len(blizcycle)][nextpos[0]][nextpos[1]]==0):
                new_state=(nextpos,(i+1)%len(blizcycle))
                if not new_state in next_positions:
                    next_positions.append(new_state)
        if nextpos == start_pos:
            break
    if nextpos == start_pos:
        break
    positions=next_positions
positions=[(start_pos,count%len(blizcycle))]
while True:
    next_positions=[]
    count += 1
    for pos, i in positions:
        for nextpos in positionsoptions(pos):
            if nextpos==goal_pos:
                break
            if validposition(nextpos) and (nextpos==start_pos or nextpos==goal_pos  or blizcycle[(i+1)%len(blizcycle)][nextpos[0]][nextpos[1]]==0):
                new_state=(nextpos,(i+1)%len(blizcycle))
                if not new_state in next_positions:
                    next_positions.append(new_state)
        if nextpos == goal_pos:
            break
    if nextpos == goal_pos:
        break
    positions=next_positions

print(count)

#311