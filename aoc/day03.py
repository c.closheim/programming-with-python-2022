#read lines of the file
with open("day03.txt", "rt") as myfile:
    rucksacks = myfile.readlines()

#part1
scores=0
for ruck in rucksacks:
    ruck = ruck.strip()
    compart1 = ruck[:len(ruck) // 2] # split the parts
    compart2 = ruck[len(ruck) // 2:]
    share=set(compart1).intersection(set(compart2)) # check the intersections
    for s in share:
        if s.isupper():
            scores+=ord(s) - 38 # Convert to numer for Upper cases
        else:
            scores+=ord(s)- 96 # Convert to numer for Lower cases
print(scores) # 8240



#part2
ruck1=[r.strip() for i, r in enumerate(rucksacks) if i%3==0] # build the rucksacks modulo
ruck2=[r.strip() for i, r in enumerate(rucksacks) if i%3==1]
ruck3=[r.strip() for i, r in enumerate(rucksacks) if i%3==2]
scores=0
for r1,r2,r3 in zip(ruck1,ruck2,ruck3): # zip the rucksacks
    share = set(r1).intersection(set(r2)).intersection(set(r3)) # check the intersections in all 3 by set convert
    for s in share:
        if s.isupper():
            scores+=ord(s) - 38 # Convert to numer for Upper cases
        else:
            scores+=ord(s) - 96 # Convert to numer for Lower cases
print(scores) # 2587