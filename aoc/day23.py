with open("day23.txt", "rt") as myfile:
   file = myfile.readlines()


class elve:
    def __init__(self, pos):
        self.pos=pos
        self.checklist=[self.check_north, self.check_south, self.check_west, self.check_east]
        self.target_position =None
    def row_pos(self):
        return self.pos[0]
    def col_pos(self):
        return self.pos[1]

    def check_position(self,map, tagmap):
        if not self.no_adjacent(map):
            for check in self.checklist:
                free,newpos=check(map)
                if free:
                    self.target_position=newpos
                    tagmap[newpos[0]][newpos[1]]+=1
                    return tagmap
        return tagmap



    def move(self, tagmap):
        if self.target_position != None and tagmap[self.target_position[0]][self.target_position[1]]==1:
            self.pos=self.target_position
        self.target_position = None
    def rotated_checlist(self):
        first=self.checklist.pop(0)
        self.checklist.append(first)

    def no_adjacent(self, map):
        return map[self.row_pos()-1][self.col_pos()-1]==0 and map[self.row_pos()-1][self.col_pos()]==0    and  \
               map[self.row_pos()-1][self.col_pos()+1]==0 and map[self.row_pos() + 1][self.col_pos() - 1] == 0 and map[self.row_pos() + 1][self.col_pos()] == 0 and \
               map[self.row_pos() + 1][self.col_pos() + 1] == 0 and map[self.row_pos()][self.col_pos()-1] == 0 and map[self.row_pos()][self.col_pos()+1] == 0

    def check_north(self, map):
        return map[self.row_pos()-1][self.col_pos()-1]==0 and map[self.row_pos()-1][self.col_pos()]==0    and  \
               map[self.row_pos()-1][self.col_pos()+1]==0, (self.row_pos()-1,self.col_pos())
    def check_south(self, map):
        return map[self.row_pos() + 1][self.col_pos() - 1] == 0 and map[self.row_pos() + 1][self.col_pos()] == 0 and \
               map[self.row_pos() + 1][self.col_pos() + 1] == 0, (self.row_pos() + 1, self.col_pos())
    def check_west(self, map):
        return map[self.row_pos() - 1][self.col_pos() -1] == 0 and map[self.row_pos()][self.col_pos()-1] == 0 and \
               map[self.row_pos() + 1][self.col_pos() - 1] == 0, (self.row_pos(), self.col_pos()-1)
    def check_east(self, map):
        return map[self.row_pos() - 1][self.col_pos() +1] == 0 and map[self.row_pos()][self.col_pos()+1] == 0 and \
               map[self.row_pos() + 1][self.col_pos() + 1] == 0, (self.row_pos(), self.col_pos()+1)


elves=[]
for r, line in enumerate(file):
    line=line.strip()
    for c,char in enumerate(line):
        if char=='#':
            elves.append(elve((r+1,c+1)))
width= c + 3
height=r+3
positionmap=[[0 for _ in range(width)] for _ in range(height)]
for elve in elves:
    positionmap[elve.row_pos()][elve.col_pos()]=1


#part 1
for _ in range(10):
    tagmap=[[0 for _ in range(width)] for _ in range(height)]
    for elve in elves:
        tagmap=elve.check_position(positionmap,tagmap)


    new_row_bevore=False
    new_row_after=False
    new_col_left=False
    new_col_right=False
    for elve in elves:
        elve.move(tagmap)
        elve.rotated_checlist()
        if elve.row_pos()==0:
            new_row_bevore=True
        if elve.row_pos()==height-1:
            new_row_after=True
        if elve.col_pos()==0:
            new_col_left=True
        if elve.col_pos()==width-1:
            new_col_right=True

    if new_row_bevore:
        height+=1
    if new_row_after:
        height += 1
    if new_col_left:
        width+=1
    if new_col_right:
        width+=1

    newpositionmap = [[0 for _ in range(width)] for _ in range(height)]

    for elve in elves:
        if new_row_bevore and new_col_left:
            elve.pos=(elve.pos[0]+1,elve.pos[1]+1)
        elif new_row_bevore:
            elve.pos = (elve.pos[0] + 1, elve.pos[1])
        elif new_col_left:
            elve.pos = (elve.pos[0], elve.pos[1] + 1)
        newpositionmap[elve.row_pos()][elve.col_pos()] = 1

    positionmap=newpositionmap

left=width
up=height
right=0
down=0
for elve in elves:
    left=min(left, elve.col_pos())
    up = min(up, elve.row_pos())
    right=max(right,elve.col_pos())
    down = max(down, elve.row_pos())

print((down-up+1)*(right-left+1)-len(elves))

#part 2
i=10
while True:
    tagmap = [[0 for _ in range(width)] for _ in range(height)]
    for elve in elves:
        tagmap = elve.check_position(positionmap, tagmap)

    new_row_bevore = False
    new_row_after = False
    new_col_left = False
    new_col_right = False
    for elve in elves:
        elve.move(tagmap)
        elve.rotated_checlist()
        if elve.row_pos() == 0:
            new_row_bevore = True
        if elve.row_pos() == height - 1:
            new_row_after = True
        if elve.col_pos() == 0:
            new_col_left = True
        if elve.col_pos() == width - 1:
            new_col_right = True

    if new_row_bevore:
        height += 1
    if new_row_after:
        height += 1
    if new_col_left:
        width += 1
    if new_col_right:
        width += 1

    newpositionmap = [[0 for _ in range(width)] for _ in range(height)]

    for elve in elves:
        if new_row_bevore and new_col_left:
            elve.pos = (elve.pos[0] + 1, elve.pos[1] + 1)
        elif new_row_bevore:
            elve.pos = (elve.pos[0] + 1, elve.pos[1])
        elif new_col_left:
            elve.pos = (elve.pos[0], elve.pos[1] + 1)
        newpositionmap[elve.row_pos()][elve.col_pos()] = 1
    i += 1
    if positionmap== newpositionmap:
        break
    else:
        positionmap = newpositionmap


print(i)



