import copy

# read the file
with open("day17.txt", "rt") as myfile:
   file = myfile.read()
file=file.strip()

moves=[]

def check_right_free(listpart):
    for list in listpart:
        if list[-1]==2:
            return False
        else:
            for l1,l2 in zip(list[:-1],list[1:]):
                if l1==2 and l2==1:
                    return False
    return True

def check_left_free(listpart):
    for list in listpart:
        if list[0]==2:
            return False
        else:
            for l1,l2 in zip(list[:-1],list[1:]):
                if l1==1 and l2==2:
                    return False
    return True

def check_down_free(listpart):
    for list1,list2 in zip(listpart[:-1],listpart[1:]):
        for l1, l2 in zip(list1,list2):
            if l1==1 and l2==2:
                return False
    return True


def filter_pattern(listpart):
    patternlist=[]
    for list in listpart:
        row=[]
        for l in list:
            if l==2:
                row.append(2)
            else:
                row.append(0)
        patternlist.append(row)
    return patternlist

def pattern_right(patternlist):
    for i in range(len(patternlist)):
        patternlist[i].insert(0,0)
        patternlist[i]=patternlist[i][:-1]
    return patternlist

def pattern_left(patternlist):
    for i in range(len(patternlist)):
        patternlist[i].append(0)
        patternlist[i] = patternlist[i][1:]
    return patternlist

def pattern_down(patternlist):
    patternlist.append([0,0,0,0,0,0,0])
    return patternlist[1:]

def move_right(listpart):
    if check_right_free(listpart):
        patternold=filter_pattern(copy.deepcopy(listpart))
        patternnew=pattern_right(copy.deepcopy(patternold))
        for i in range(len(listpart)):
            for j in range(len(listpart[i])):
                listpart[i][j]=listpart[i][j]-patternold[i][j]+patternnew[i][j]

    return listpart

def move_left(listpart):
    if check_left_free(listpart):
        patternold=filter_pattern(copy.deepcopy(listpart))
        patternnew=pattern_left(copy.deepcopy(patternold))
        for i in range(len(listpart)):
            for j in range(len(listpart[i])):
                listpart[i][j] = listpart[i][j] - patternold[i][j] + patternnew[i][j]

    return listpart

def move_down(listpart):
    if check_down_free(listpart):
        patternold=filter_pattern(copy.deepcopy(listpart))
        patternnew=pattern_down(copy.deepcopy(patternold))
        for i in range(len(listpart)):
            for j in range(len(listpart[i])):
                listpart[i][j] = listpart[i][j] - patternold[i][j] + patternnew[i][j]
        return listpart, False
    else:
        for row in range(len(listpart)):
            for col in range(len(listpart[row])):
                if listpart[row][col]==2:
                    listpart[row][col] =1
        return listpart, True

def proceed(stack, stepcounter):
    window = -5
    rock_stopped = False
    while not rock_stopped:
        jet=moves[stepcounter]
        if jet==0:
           stack[window:]=move_left(copy.deepcopy(stack[window:]))
        else:
            stack[window:]=move_right(copy.deepcopy(stack[window:]))
        stack[window:], rock_stopped=move_down(copy.deepcopy(stack[window:]))
        if stack[-1]==[0,0,0,0,0,0,0]:
            stack=stack[:-1]
        else:
            window-=1
        stepcounter =(stepcounter+1)%len(moves)
    return stack, stepcounter

# transfere the moves
for char in file:
    if char=='<':
        moves.append(0)
    elif char=='>':
        moves.append(1)


empyt=[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]] # define the empty grid
pattern=[[[0,0,2,2,2,2,0]], # the -
         [[0,0,0,2,0,0,0],[0,0,2,2,2,0,0],[0,0,0,2,0,0,0]], # the +
         [[0,0,2,2,2,0,0],[0,0,0,0,2,0,0],[0,0,0,0,2,0,0]], # the _|
         [[0,0,2,0,0,0,0],[0,0,2,0,0,0,0],[0,0,2,0,0,0,0],[0,0,2,0,0,0,0]], # the |
         [[0,0,2,2,0,0,0],[0,0,2,2,0,0,0]]] # the o

#part 1
stack=[[1,1,1,1,1,1,1]] # the base line
stepcounter=0
for i in range(2022): # move 2022 times
    rock=pattern[i%len(pattern)] # take the pattern
    stack.extend(empyt) # extent the stack
    stack.extend(rock) # but the pattern on top
    stack, stepcounter=proceed(stack,stepcounter) # put the pattern down
print(len(stack)-1) # 3235



#part 2
cycle=[]
height={}
stack=[[1,1,1,1,1,1,1]]
stepcounter=0
stackrest=0
maxcount=1000000000000
for i in range(maxcount):
    if(i % len(pattern) ==0):
        draft=(i% len(pattern), stepcounter)
        if cycle.count(draft)==2:
              stonecount=len(pattern)* int((len(cycle)- cycle.index(draft))/2) # take away the useless stones
              multiple= int((maxcount-height[draft][0])/stonecount)-2
              height_to_add=(len(stack)-height[draft][1])*multiple
              stonesskip=multiple*stonecount
              break
        else:
            cycle.append(draft)
            height[draft]=(i,len(stack))

    rock=pattern[i%len(pattern)]
    stack.extend(empyt)
    stack.extend(rock)

    stack, stepcounter=proceed(stack,stepcounter)



for i in range(maxcount-i-stonesskip):
    rock=pattern[i%len(pattern)]
    stack.extend(empyt)
    stack.extend(rock)
    stack, stepcounter=proceed(stack,stepcounter)

print(height_to_add+len(stack)-1) #1591860465110